<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '8430ffadd122d80c10a5d659566ff0e3251bf3b6',
    'name' => 'topthink/think',
  ),
  'versions' => 
  array (
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v6.1.2',
      'version' => '6.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c297139da7c6873dbd67cbd1093f09ec0bbd0c50',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '094defdb4a7001845300334e7c1ee2335925ef99',
    ),
    'league/flysystem-cached-adapter' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd1925efb2207ac4be3ad0c40b8277175f99ffaff',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ff6248ea87a9f116e78edd6002e39e5128a0d4dd',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4407588e0d3f1f52efb65fbe92babe41f37fe50c',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v4.4.41',
      'version' => '4.4.41.0',
      'aliases' => 
      array (
      ),
      'reference' => '58eb36075c04aaf92a7a9f38ee9a8b97e24eb481',
    ),
    'topthink/framework' => 
    array (
      'pretty_version' => 'v6.0.12',
      'version' => '6.0.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e478316ac843c1a884a3b3a7a94db17c4001ff5c',
    ),
    'topthink/think' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '8430ffadd122d80c10a5d659566ff0e3251bf3b6',
    ),
    'topthink/think-helper' => 
    array (
      'pretty_version' => 'v3.1.6',
      'version' => '3.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
    ),
    'topthink/think-orm' => 
    array (
      'pretty_version' => 'v2.0.52',
      'version' => '2.0.52.0',
      'aliases' => 
      array (
      ),
      'reference' => '407a60658f37fc57422ab95a9922c6f69af90f46',
    ),
    'topthink/think-trace' => 
    array (
      'pretty_version' => 'v1.4',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a9fa8f767b6c66c5a133ad21ca1bc96ad329444',
    ),
    'workerman/channel' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3df772d0d20d4cebfcfd621c33d1a1ab732db523',
    ),
    'workerman/globaldata' => 
    array (
      'pretty_version' => 'v1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'baf3b7af338f329e88136ebe92266dfe40dd00a7',
    ),
    'workerman/mysql' => 
    array (
      'pretty_version' => 'v1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '28272aa68f9ea1a482f9bb0cf709d169f772d228',
    ),
    'workerman/phpsocket.io' => 
    array (
      'pretty_version' => 'v1.1.14',
      'version' => '1.1.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a5758da4d55b4744a4cc9c956816d88ce385601e',
    ),
    'workerman/workerman' => 
    array (
      'pretty_version' => 'v4.0.36',
      'version' => '4.0.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ecf8b791f1a4beb98c1e0e9d09988cd4b0d1c5ab',
    ),
    'zoujingli/ip2region' => 
    array (
      'pretty_version' => 'v1.0.12',
      'version' => '1.0.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '82cebc7a6be46524797454e98d3b165521065c26',
    ),
  ),
);
